// Fill out your copyright notice in the Description page of Project Settings.


#include "TankTrack.h"
#include "SprungWheel.h"
#include "SpawnPoint.h"

UTankTrack::UTankTrack()
{
    PrimaryComponentTick.bCanEverTick = true;
    
}

void UTankTrack::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    CurrentThrottle = 0;
}


void UTankTrack::SetThrottle(float Throttle){
    CurrentThrottle += Throttle;
    DriveTrack(CurrentThrottle);
}

void UTankTrack::DriveTrack(float Throttle)
{
    auto ForceApplied = Throttle * MaxDrivingForce;
    auto Wheels = GetWheels();
    auto ForcePerWheel = ForceApplied / Wheels.Num();
    for (ASprungWheel* Wheel : Wheels)
    {
        if(Wheel){
        Wheel ->AddDrivingForce(ForceApplied);
        }
    }
}

TArray<ASprungWheel*> UTankTrack::GetWheels() const 
{
    TArray<USceneComponent*> SpawnPoints;

    this ->GetChildrenComponents(false, SpawnPoints);

    TArray<ASprungWheel*> Wheels;

    for(USceneComponent* SpawnPoint : SpawnPoints)
    {
        if(!SpawnPoint){continue;}
        ASprungWheel* Wheel = Cast<USpawnPoint>(SpawnPoint) -> GetSprungWheel();
        if(!Wheel){continue;}
        Wheels.Add(Wheel);
    }
    
    return Wheels;
}