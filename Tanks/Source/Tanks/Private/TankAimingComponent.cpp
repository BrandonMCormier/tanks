// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAimingComponent.h"
#include "TankBarrel.h"
#include "TankShell.h"
#include "Turret.h"
#include "Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"


// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UTankAimingComponent::TickComponent(float DeltaTime,enum ELevelTick TickType,FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(ShellCount <= 0){
		FiringState = EFiringState::OutOfAmmo;
	}
	else if ((FPlatformTime::Seconds() - LastFireTime) < ReloadTimeInSeconds){
		FiringState = EFiringState::Reloading;
	}
	else if(IsBarrelMoving()){
		FiringState = EFiringState::Aiming;
	}
	else{
		FiringState = EFiringState::Locked;
	}
	//UE_LOG(LogTemp, Warning, TEXT("FiringState: %i"), FiringState)
}

void UTankAimingComponent::Initialize(UTankBarrel* BarrelToSet, UTurret* TurretToSet)
{
	if(!BarrelToSet || !TurretToSet){return;}

	SetBarrelReference(BarrelToSet);
	SetTurretReference(TurretToSet);
}

void UTankAimingComponent::SetBarrelReference(UTankBarrel* BarrelToSet){
	Barrel = BarrelToSet;
	//UE_LOG(LogTemp, Warning, TEXT("%s"), *Barrel->GetName());
}

void UTankAimingComponent::SetTurretReference(UTurret* TurretToSet){
	Turret = TurretToSet;
	//UE_LOG(LogTemp, Warning, TEXT("%s"), *Barrel->GetName());
}


// Called when the game starts
void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();

	LastFireTime = FPlatformTime::Seconds();
	
}




void UTankAimingComponent::AimAt(FVector HitLocation){

	if (!Barrel) { return;}
	auto OurTankName = GetOwner() -> GetName();
	FVector TossVelocity(0);
	if (UGameplayStatics::SuggestProjectileVelocity(
		this, 
		TossVelocity, 
		Barrel->GetSocketLocation(FName("Projectile")), 
		HitLocation, 
		LaunchSpeed,
		false,
		0.0f,
		0.0f,
		ESuggestProjVelocityTraceOption::DoNotTrace
		))
	{
		AimDirection = TossVelocity.GetSafeNormal();
		
		MoveBarrel();
		MoveTurret();
		//UE_LOG(LogTemp, Warning, TEXT("AimAt: %s"), *AimDirection.ToString());
	}
	else{
		//UE_LOG(LogTemp, Warning, TEXT("Solution Not Found"));
	}
}

void UTankAimingComponent::Fire(){
	
	
	if (FiringState == EFiringState::Locked || FiringState == EFiringState::Aiming){
		ShellCount--;
		auto Shell = GetWorld()->SpawnActor<ATankShell>(ShellBP, 
										Barrel -> GetSocketLocation(FName("Projectile")), 
										Barrel -> GetSocketRotation(FName("Projectile")),
										FActorSpawnParameters());

		Shell ->Launch(LaunchSpeed);
		LastFireTime = FPlatformTime::Seconds();
	}
}

void UTankAimingComponent::MoveBarrel(){
	auto BarrelRotator = Barrel -> GetForwardVector().Rotation();
	auto AimAsRotator = AimDirection.Rotation();
	auto DeltaRotator = AimAsRotator - BarrelRotator;

	Barrel -> Elevate(DeltaRotator.Pitch); // TODO remove magic number

	
}

void UTankAimingComponent::MoveTurret(){
	auto TurretRotator = Turret -> GetForwardVector().Rotation();
	auto AimAsRotator = AimDirection.Rotation();
	auto DeltaRotator = AimAsRotator - TurretRotator;

	if(DeltaRotator.Yaw > 180.f || DeltaRotator.Yaw < -180.f){
		DeltaRotator.Yaw *= -1; //Always use shortest path
	}


	Turret -> Rotate(DeltaRotator.Yaw);

	
}


bool UTankAimingComponent::IsBarrelMoving()
{
	return !Barrel ->GetForwardVector().Equals(AimDirection, 0.01f);
}

EFiringState UTankAimingComponent::GetFiringState()
{
	return FiringState;
}


