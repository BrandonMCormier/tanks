// Fill out your copyright notice in the Description page of Project Settings.


#include "TankMovementComponent.h"
#include "TankTrack.h"

void UTankMovementComponent::Initialize(UTankTrack* LeftTrackToSet, UTankTrack* RightTrackToSet){
     
     if(!LeftTrackToSet || !RightTrackToSet){return;}
     
     LeftTrack = LeftTrackToSet;
     RightTrack = RightTrackToSet;
}

void UTankMovementComponent::IntendMoveForward(float Throw){

     if(!LeftTrack || !RightTrack){return;}
     LeftTrack ->SetThrottle(Throw);
     RightTrack -> SetThrottle(Throw);
     //TODO: Stop compunding conrtols getting speed higher than intended. 
     
}

void UTankMovementComponent::IntendMoveRight(float Throw){

     if(!LeftTrack || !RightTrack){return;}
     LeftTrack ->SetThrottle(Throw);
     RightTrack -> SetThrottle(-Throw);
}

void UTankMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed){
     //No Need for super, we are replacing functionality

     
     auto AIForwardIntention = MoveVelocity.GetSafeNormal();
     auto TankForwardDirection = GetOwner() -> GetActorForwardVector().GetSafeNormal();
     
     auto DotProduct = FVector::DotProduct(AIForwardIntention, TankForwardDirection);

     IntendMoveForward(DotProduct);

     auto RightThrow = FVector::CrossProduct(TankForwardDirection, AIForwardIntention).Z;

     IntendMoveRight(RightThrow);

}