// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"
#include "Tank.h"
#include "TankAimingComponent.h"
#include "Tank.h"




void ATankAIController::BeginPlay()
{
    Super::BeginPlay();

    PlayerTank = Cast<ATank>(GetWorld() -> GetFirstPlayerController() -> GetPawn());
    AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
    
}

void ATankAIController::Tick(float DeltaTime){
    Super::Tick(DeltaTime);
    
    
    

    if(PlayerTank){
        
        MoveToActor(PlayerTank, AcceptanceRadius);

        AimingComponent->AimAt(PlayerTank->GetActorLocation());

        if(AimingComponent -> GetFiringState() == EFiringState::Locked)
        {
            AimingComponent->Fire();
        }
    }

}

void ATankAIController::SetPawn(APawn* InPawn)
{
    Super::SetPawn(InPawn);
    if (InPawn)
    {
        auto PossessedTank = Cast<ATank>(InPawn);
        if (!ensure(PossessedTank)) {return;}
        //subscribe our local method to the tank's death event TODO.
        PossessedTank ->OnTankDeath.AddUniqueDynamic(this, &ATankAIController::OnPossessedTankDeath);

    }
    
}

void ATankAIController::OnPossessedTankDeath()
{
    GetPawn()->DetachFromControllerPendingDestroy();
    UE_LOG(LogTemp, Warning, TEXT("I HAVE DIED!!!"));
}

