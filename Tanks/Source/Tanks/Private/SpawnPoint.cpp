// Made By Brandon Cormier for the GameDev.TV Udemy Unreal C++ Course


#include "SpawnPoint.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "SprungWheel.h"

// Sets default values for this component's properties
USpawnPoint::USpawnPoint()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USpawnPoint::BeginPlay()
{
	Super::BeginPlay();

	WheelSystem = GetWorld() -> SpawnActorDeferred<ASprungWheel>(SpawnClass, GetComponentTransform());
	if(!WheelSystem){return;}
	WheelSystem -> AttachToComponent(this, FAttachmentTransformRules::KeepWorldTransform);
	UGameplayStatics::FinishSpawningActor(WheelSystem, GetComponentTransform());
}


// Called every frame
void USpawnPoint::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

ASprungWheel* USpawnPoint::GetSprungWheel() const
{
	return WheelSystem;
}

